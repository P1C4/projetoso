var indexSectionsWithContent =
{
  0: "abcdefghilmnprstuv",
  1: "abcfgiptu",
  2: "rs",
  3: "abcdefgilmprt",
  4: "acefhimnptv",
  5: "fu",
  6: "s"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Pages"
};

