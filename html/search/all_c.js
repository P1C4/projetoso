var searchData=
[
  ['pc',['pc',['../structAtual.html#a33aa34a8e7acba8e341dc18b65d9f266',1,'Atual']]],
  ['pcbtable',['pcbTable',['../structGestor.html#ab07d193284c16f063968d98e68c3506a',1,'Gestor']]],
  ['pid',['pid',['../structUnidade__bloco.html#a985cd701f1aa7684b4e8757b5252c9f3',1,'Unidade_bloco::pid()'],['../structProcesso.html#a357c3ffeb8d4ed658515d9fae7c1b35c',1,'Processo::pid()'],['../structFila.html#a44e3a7e7d0524762c663133e9784d904',1,'Fila::pid()'],['../structAtual.html#a4e60add4ce5bbe0f98d178e4d474eb86',1,'Atual::pid()']]],
  ['ppid',['ppid',['../structProcesso.html#a1cc776dfd8860dff06290cc9e00713f5',1,'Processo::ppid()'],['../structAtual.html#ab6f9aaf7068642dba90cc967ad3c0213',1,'Atual::ppid()']]],
  ['print_5fglobal_5fstats',['print_Global_Stats',['../simulador_8c.html#a865a8fcb6003acc9b6411e28d95b946e',1,'simulador.c']]],
  ['priority',['priority',['../structProcesso.html#a1c595d29aebc89dd60c3263b7e46a2a1',1,'Processo::priority()'],['../structControl.html#abb999dafd83c4fbf2135875f79d4273f',1,'Control::priority()'],['../structAtual.html#ace2a6fb94d0f8b54446924f11f82d4ff',1,'Atual::priority()']]],
  ['processo',['Processo',['../structProcesso.html',1,'Processo'],['../structBloco.html#a4e5f4002db8bc3612b5ebfd56a6d963f',1,'Bloco::processo()'],['../structFila.html#a3f09665dc21d0b48839505e83fab5a83',1,'Fila::processo()']]],
  ['processo_5fatual',['processo_atual',['../structGestor.html#ac3d5e873d72304b0732a24cb0a817325',1,'Gestor']]],
  ['processos_5fa_5fcorrer',['processos_a_correr',['../structGestor.html#a0dfc106cc9799bd268db161283fdcd8b',1,'Gestor']]],
  ['processos_5fbloqueados',['processos_bloqueados',['../structGestor.html#af1dc95c1f73c60cbc87628dd9f744f1e',1,'Gestor']]],
  ['processos_5fconcorrentes',['processos_concorrentes',['../structGestor.html#a7fb8d2f296b5314d43e1292163fb7b28',1,'Gestor']]],
  ['processos_5fterminados',['processos_terminados',['../structGestor.html#a311d06dec4985e66d0632df7eb218d8e',1,'Gestor']]],
  ['prontos',['prontos',['../structGestor.html#a3d78cff8f204cd30c9ea7b5b09fe0108',1,'Gestor']]]
];
