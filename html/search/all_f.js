var searchData=
[
  ['t',['t',['../simulador_8c.html#a8c916e772de92782455c9352cf1fe180',1,'simulador.c']]],
  ['tamanho',['tamanho',['../structUnidade__bloco.html#a37e0234323f9ceb61943b8984fc027c0',1,'Unidade_bloco']]],
  ['tamanho_5fna_5fmemoria',['tamanho_na_memoria',['../structProcesso.html#af4f490cb67123919c4cc939b572f457a',1,'Processo']]],
  ['tamanho_5fno_5fheap',['tamanho_no_heap',['../structProcesso.html#a1159edde35f28f97a1b6c5ffb55374d8',1,'Processo']]],
  ['tamanho_5focupado',['tamanho_ocupado',['../structBloco.html#abe51d328ca9efa932b45f73c1de97809',1,'Bloco']]],
  ['tamanho_5frestante',['tamanho_restante',['../structBloco.html#a6a16cf453db4abb48f0633a163635d76',1,'Bloco']]],
  ['tamanho_5ftotal',['tamanho_total',['../structBloco.html#a6239c5874c1963c700e49f65a2b01718',1,'Bloco']]],
  ['tempo',['tempo',['../structGestor.html#a170fd0ef11f70f8713f95d71b420d7fe',1,'Gestor']]],
  ['tempo_5fchegada',['tempo_chegada',['../structControl.html#a3576daf6d19343d199705dc106041959',1,'Control']]],
  ['tempo_5frestante',['tempo_restante',['../structProcesso.html#a7e6b995bd815686b871c09c5b068f42b',1,'Processo::tempo_restante()'],['../structAtual.html#aa669edacbd0db90361335b50e80e5016',1,'Atual::tempo_restante()']]],
  ['tempo_5fusado',['tempo_usado',['../structProcesso.html#a941084f2f1323f22959358bed7319a0e',1,'Processo::tempo_usado()'],['../structAtual.html#a91ccffe595ee0258c3033be82de75043',1,'Atual::tempo_usado()']]],
  ['time_5fquantum',['time_quantum',['../simulador_8c.html#a0a66155809279ce663ad6d55e313d220',1,'simulador.c']]],
  ['trocar',['trocar',['../simulador_8c.html#a5b4ad7a8822801d4842b9985ac34ac09',1,'simulador.c']]],
  ['turnaround_5ftime',['turnaround_time',['../simulador_8c.html#a57c5a23aee716876a8245af54569325a',1,'simulador.c']]],
  ['turnarounds',['Turnarounds',['../structTurnarounds.html',1,'']]]
];
