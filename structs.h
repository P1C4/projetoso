
typedef struct Unidade_bloco
{
	int pid;
	int tamanho;
	struct Unidade_bloco *nseg;
}UNIDADE_BLOCO;

typedef struct
{
	int tamanho_total;
	int tamanho_ocupado;
	int tamanho_restante;
	UNIDADE_BLOCO *processo;
}Bloco;

/**
 * @brief Struct que guarda os dados de um processo
 * 
 */
typedef struct
{
  char fname[50]; // Nome
  int pid;
  int ppid;
  int valor;
  int priority;
  int chegada;
  int tempo_usado;
  int tempo_restante;
  int tamanho_na_memoria[2];
  int tamanho_no_heap;

}Processo;


/**
 * @brief Struct que guarda os dados de uma instrução
 * 
 */
typedef struct { 
    char ins; // Nome da instrução
    int n ; // Valor da instrução
    char nome[50];  // -> Serve para o filename
} Instrucao;




/**
 * @brief Struct que guarda uma lista de processos
 * 
 */
typedef struct Fila {
  int pid;
  Processo processo;
  struct Fila *nseg; 
} FILA; // Fila de processos


/**
 * @brief Struct que guarda os processos com os seus dados de chegada e prioridade
 * 
 */
typedef struct{

  char name[50];
  int tempo_chegada;
  int priority;

}Control; // Usamos esta estrutura para ver os processos e os seus respetivos tempos de chegada


/**
 * @brief Struct que guarda o processo atual
 * 
 */
typedef struct
{
  int pc;
  int pid;
  int ppid;
  int priority;
  int chegada;
  int valor;
  int tempo_usado;
  int tempo_restante;
}Atual; //Usamos esta estrutura para controlar o processo atual



/**
 * @brief Struct que guarda os dados do turn around time
 * 
 */
typedef struct{
  int c;
  int array_tempos[64];

}Turnarounds; // Usamos esta estrutura para calcular os turn around times e para mais tarde calcularmos a media de todos


/**
 * @brief Gestor de processos
 * 
 */
typedef struct{

  int tempo;
  Atual processo_atual;
  Processo pcbTable[100];
  FILA *processos_a_correr;
  FILA *prontos;
  FILA *processos_bloqueados;
  FILA *processos_concorrentes;
  FILA *processos_terminados;


}Gestor; 

