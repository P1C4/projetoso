/**
 * @file simulador.c
 * @author Pedro Brito | António Sardinha | Mauro Gaudêncio 
 * @brief Ficheiro principal do projeto de SO
 * @version 1
 * @date 2020-06-5
 * 
 * @copyright Copyright (c) 2020
 * 
 */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "structs.h"
#include <pthread.h>
#include <time.h>
#include <signal.h>

//Variavel que controla o algoritmo de escalonamento
int escolha_escalonamento = 0; // Default Round Robin

//Variavel que controla o algoritmo de aloca��o de memoria
int escolha_alocacao = 0; //Default Best Fit

//Variavel que controla a memória alocada
int contador_memoria = 0;

//Variável que controla o numero de processos
int contador_processos = 0;

//Controlar o pid dos processos
int current_pid = 0;

Bloco Heap[5];

int index_next_fit = 0;

//Numero de comandos a executar
int numero_de_comandos = 0;

//N unidades de tempo que o simulador irá executar um processo por cada instrução E
int time_quantum = 2;

//Variável memória que guarda as instruções de cada processo
Instrucao *memory;

//Variável que contém todos os processos com os seus tempos de chegada e prioridades
Control *control_processos;

//Contém todos os comandos a executar
char *comandos;

//Usado para gerar varias seeds
time_t t;

/**
 * @brief Copiamos um processo e retornamos a cópia
 * 
 * @param p Processo a copiar
 * @return Cópia do processo
 */
Processo copiarProcesso(Processo *p)
{
	Processo aux;

	strcpy(aux.fname, p->fname);
	aux.pid = p->pid;
	aux.ppid = p->ppid;
	aux.valor = p->valor;
	aux.priority = p->priority;
	aux.chegada = p->chegada;
	aux.tempo_usado = p->tempo_usado;
	aux.tempo_restante = p->tempo_restante;
	aux.tamanho_na_memoria[0] = p->tamanho_na_memoria[0];
	aux.tamanho_na_memoria[1] = p->tamanho_na_memoria[1];

	return aux;
}

/**
 * @brief Insere um processo na fila

 * 
 * @param f Fila de processos
 * @param pid Pid a adicionar
 * @param p Processo a adicionar
 * @return FILA* Fila com novo elemento adicionado
 */
FILA *Inserir_fila(FILA **f, int pid, Processo p)
{
	FILA *aux = (FILA *)malloc(sizeof(FILA));

	aux->pid = pid;
	aux->processo = p;
	aux->nseg = *f;

	*f = aux;

	return aux;
}

/**
 * @brief Insere um processo no final da fila

 *
 * @param f Fila de processos
 * @param pid Pid a adicionar
 * @param p Processo a adicionar
 * @return FILA* Fila com novo elemento adicionado
 */
FILA *Inserir_last(FILA **f, int pid, Processo p)
{
	FILA *aux = *f;

	if (aux == NULL)
		return Inserir_fila(f, pid, p);

	while (aux->nseg != NULL)
		aux = aux->nseg;

	FILA *aux2 = (FILA *)malloc(sizeof(FILA));

	aux2->nseg = aux->nseg;
	aux->nseg = aux2;
	aux2->pid = pid;
	aux2->processo = p;

	return aux2;
}

void copiarFila(FILA **f, FILA **c)
{
	FILA *aux = *f;

	while (aux != NULL)
	{
		Inserir_last(c, aux->pid, aux->processo);
		aux = aux->nseg;
	}
}

/**
 * @brief Trocamos o elemento A com o B

 * 
 * @param A Elemento de troca A
 * @param B Elemento de troca B
 */
void trocar(FILA *A, FILA *B)

{
	FILA *aux;
	Inserir_fila(&aux, A->pid, A->processo);

	aux->pid = A->pid;
	aux->processo = A->processo;

	A->pid = B->pid;
	A->processo = B->processo;

	B->pid = aux->pid;
	B->processo = aux->processo;
}

//Referencia -> https://www.geeksforgeeks.org/c-program-bubble-sort-linked-list/
/**
 * @brief Bubble sort para organizar por pids

 *
 * @param head -> Recebe a lista para organizar
 */
void bubbleSortPID(FILA *head)
{

	int controlar;
	FILA *aux;
	FILA *aux2 = NULL;
	/* Checking for empty list */
	if (head == NULL)
		return;
	do
	{
		controlar = 0;
		aux = head;

		while (aux->nseg != aux2)
		{
			if (aux->processo.pid > aux->nseg->processo.pid)
			{
				trocar(aux, aux->nseg);
				controlar = 1;
			}
			aux = aux->nseg;
		}
		aux2 = aux;
	} while (controlar);
}

/**
 * @brief Bubble sort para organizar por prioridade

 * 
 * @param head Recebe a lista a organizar
 */
void bubbleSortPrio(FILA *head)
{

	int controlar;
	FILA *aux;
	FILA *aux2 = NULL;
	/* Checking for empty list */
	if (head == NULL)
		return;
	do
	{
		controlar = 0;
		aux = head;

		while (aux->nseg != aux2)
		{
			if (aux->processo.priority > aux->nseg->processo.priority)
			{
				trocar(aux, aux->nseg);
				controlar = 1;
			}
			aux = aux->nseg;
		}
		aux2 = aux;

	} while (controlar);
}

/**
 * @brief Bubble sort por menor tempo 

 * 
 * @param head Recebe a lista a organizar
 */
void bubbleSortSJF(FILA *head)
{
	int controlar;
	FILA *aux;
	FILA *aux2 = NULL;
	/* Checking for empty list */
	if (head == NULL)
		return;
	do
	{
		controlar = 0;
		aux = head;

		while (aux->nseg != aux2)
		{
			if (aux->processo.tempo_restante > aux->nseg->processo.tempo_restante)
			{
				trocar(aux, aux->nseg);
				controlar = 1;
			}
			aux = aux->nseg;
		}
		aux2 = aux;
	} while (controlar);
}

/**
 * @brief Retirar primeiro processo da fila

 * 
 * @param f Rece a fila de onde retirar o elemento
 * @return Pid retirado
 */
int Retirar_elemento(FILA **f)
{
	int pid = -1;

	if (*f != NULL)
	{

		pid = (*f)->pid;
		FILA *aux = *f;
		*f = aux->nseg;
		free(aux);
		return pid;
	}
	else
	{
		return pid;
	}
}

/**
 * @brief Retira um elemento especifico da lista
 * 
 * @param f lista de onde remover
 * @param remove_pid Elemento  a remover
 * @return int 
 */
int Retirar_elemento_especifico(FILA **f, int remove_pid)
{
	FILA *aux = *f;
	FILA *aux2 = *f;

	while (aux != NULL)
	{
		if (aux->processo.pid == remove_pid)
		{
			break;
		}
		aux2 = aux;
		aux = aux->nseg;
	}

	if (aux == NULL)
	{
		return -1;
	}

	if(aux2->pid == aux->pid){
		*f = aux->nseg;
	}
	else{
		aux2->nseg = aux->nseg;
	}

	free(aux);
	return 1;
}


/**
 * @brief Insere um bloco de unidade 
 * 
 * @param pid pid a adicionar
 * @param tamanho_a_ocupar tamanaho que vai ocupar
 * @param index em que bloco introduzir
 * @return UNIDADE_BLOCO* 
 */
UNIDADE_BLOCO *Inserir_bloco(int pid, int tamanho_a_ocupar, int index){
	
	int c = Heap[index].tamanho_total;
	
	UNIDADE_BLOCO *adicionar = (UNIDADE_BLOCO *)malloc(sizeof(UNIDADE_BLOCO));
	UNIDADE_BLOCO *aux = Heap[index].processo;
	UNIDADE_BLOCO *aux2 = Heap[index].processo;
	
	while(aux != NULL){
		
		if(aux->pid == -1){ //bloco esta livre
			if(aux->tamanho == tamanho_a_ocupar){ //ha espa�o
				aux->pid = pid;
				return Heap[index].processo;
			}
			else if(aux->tamanho > tamanho_a_ocupar){
				adicionar->pid = pid;
				adicionar->tamanho = tamanho_a_ocupar;
				aux->tamanho -= tamanho_a_ocupar;
				adicionar->nseg = aux;
				if(aux == aux2){
					return adicionar;
				}
				else{
					aux2->nseg = adicionar;
					return Heap[index].processo;
				}
			}
		}
		c -= aux->tamanho;
		aux2 = aux;
		aux = aux->nseg;
	}
	
	if(tamanho_a_ocupar <= c){
		adicionar->pid = pid;
		adicionar->tamanho = tamanho_a_ocupar;
		adicionar->nseg = NULL;
		
		if(aux2 != NULL)
			aux2->nseg = adicionar;
		else
			return adicionar;
	}
	
	return Heap[index].processo;
}


/**
 * @brief conta os fragmentos que existem na memória
 * 
 * @return int 
 */
int fragment_heap(){
	int counter = 0;
	for(int i = 0 ; i < 5 ; i++){
		UNIDADE_BLOCO *aux = Heap[i].processo;
		while(aux != NULL && aux->nseg != NULL){
			if(aux->pid == -1 && aux->nseg->pid != -1){
				counter++;
			}
			aux = aux->nseg;
		}
	}
	return counter;
}



/**
 * @brief Dealoca o processo da memoria
 * 
 * @param pid pid para dealocar
 * @return int 
 */
int dealocar_heap(int pid){
	int fragmentos;
	printf("\nA dealocar para o pid = %d\n", pid);
	
	for(int i = 0; i < 5; i++){
		UNIDADE_BLOCO *aux2 = Heap[i].processo;
		UNIDADE_BLOCO *aux3 = Heap[i].processo;
		while(aux2 != NULL){
			if(aux2->pid == pid){
				Heap[i].tamanho_ocupado -= aux2->tamanho;
				Heap[i].tamanho_restante += aux2->tamanho;
				UNIDADE_BLOCO *aux = aux2;
				while(aux != NULL){
					if(aux->pid == pid){
						aux->pid = -1;
						if(aux != aux3 && aux3->pid == -1){
							aux3->tamanho += aux->tamanho;
							aux3->nseg = aux->nseg;
							aux = aux3;
						}
						if(aux->nseg !=NULL && aux->nseg->pid == -1){
							aux->tamanho += aux->nseg->tamanho;
							aux->nseg = aux->nseg->nseg;
						}
					}
					aux = aux->nseg;
				}
				printf("Sucesso\n");
				fragmentos = fragment_heap();
				printf("\nExistem %d fragmentos\n", fragmentos);
				return 1;
			}
			aux3 = aux2;
			aux2 = aux2 ->nseg;
		}
	}
	fragmentos = fragment_heap();
	printf("\nExistem %d fragmentos\n", fragmentos);
	printf("Nao consegui dealocar\n");
	return -1;
}



/**
 * @brief Gestor de memoria que trata de alocar um processo
 * 
 * @param tamanho_a_ocupar tamanho do processo a alocar
 * @param pid pid a alocar
 * @return int 
 */
int GestorMemoria(int tamanho_a_ocupar, int pid){
	printf("\nEstou a tentar alocar o processo com pid = %d\n", pid);
	int fragmentos;
	int flag = 0;
	
	
	for(int i = 0; i < 5; i++){
		if(Heap[i].tamanho_restante > tamanho_a_ocupar){
			flag = 1;
			break;
		}
	}
	
	if(flag != 0){
		int index = -1;
		switch (escolha_alocacao)
			{
			
			//Corremos o programa para N unidades de tempo
			case 1: //worst fit
				
				for(int i = 0; i < 5; i++){
					if(index == -1){
						index = i;
					}
					else if(Heap[i].tamanho_restante >= Heap[index].tamanho_restante){
						index = i;
					}
				}
				
				Heap[index].tamanho_ocupado += tamanho_a_ocupar;
				Heap[index].tamanho_restante -= tamanho_a_ocupar;
				
				Heap[index].processo = Inserir_bloco(pid, tamanho_a_ocupar, index);
				
				break;
				
			case 2: //first fit
				
				for(int i = 0; i < 5; i++){
					if(Heap[i].tamanho_restante >= tamanho_a_ocupar){
						index = i;
						break;
					}
				}
				
				if(index != -1){
					Heap[index].tamanho_ocupado += tamanho_a_ocupar;
					Heap[index].tamanho_restante -= tamanho_a_ocupar;
					
					Heap[index].processo = Inserir_bloco(pid, tamanho_a_ocupar, index);
				}
					
			
				break;
				
			case 3: //next fit
			
				if(index_next_fit == 5)
					index_next_fit = 0;
			
				for(int i = index_next_fit; i < 5; i++){
						
					if(Heap[i].tamanho_restante >= tamanho_a_ocupar){
						index = i;
						break;
					}
					
					if(i == 4)
						i = 0;
					
				}
				
				Heap[index].tamanho_ocupado += tamanho_a_ocupar;
				Heap[index].tamanho_restante -= tamanho_a_ocupar;
				
				Heap[index].processo = Inserir_bloco(pid, tamanho_a_ocupar, index);
				
				index_next_fit = index + 1;
				
				break;
				
			default: //best fit
			
				for(int i = 0; i < 5; i++){
						
					if(Heap[i].tamanho_restante >= tamanho_a_ocupar){
						
						if(index == -1)
							index = i;
						
						else if(Heap[i].tamanho_restante < Heap[index].tamanho_restante)
							index = i;
						
						break;
						
					}
				}
				
				Heap[index].tamanho_ocupado += tamanho_a_ocupar;
				Heap[index].tamanho_restante -= tamanho_a_ocupar;
			
				Heap[index].processo = Inserir_bloco(pid, tamanho_a_ocupar, index);
		}
	}
	else{
		printf("N�o h� espa�o para o processo!");
		fragmentos = fragment_heap();
		printf("\nExitem %d fragmentos\n", fragmentos);
		return -1;
	}
	printf("Alocado com sucesso!\n");
	fragmentos = fragment_heap();
	printf("\nExitem %d fragmentos\n", fragmentos);
	return 0;
}


/**
 * @brief Concatena duas strings

 * 
 * @param s1 Primeira string a concatenar
 * @param s2 Segunda string a concatenar
 * @return char*  Resultado da concatenação
 */
char *concatenar_duas_strings(const char *s1, const char *s2)
{
	char *result = malloc(strlen(s1) + strlen(s2) + 1);
	strcpy(result, s1);
	strcat(result, s2);
	return result;
}

/**
 * @brief Copiamos um processo pai para um filho que vai correr concorrentemente com o pai-> fork em linux
 * 
 * @param p Processo Pai
 * @param tempo_atual Tempo atual
 * @return Processo Retornamos o processo filho 
 */
Processo forkProcesso(Processo *p, int tempo_atual)
{
	Processo aux;

	int resultado_memoria = GestorMemoria(p->tamanho_no_heap, current_pid);
	if(resultado_memoria == -1){
		aux.pid = -2;
		return aux;
	}

	strcpy(aux.fname, p->fname);
	aux.pid = current_pid;
	current_pid++;
	aux.ppid = p->pid;
	aux.valor = p->valor;
	aux.priority = p->priority;
	aux.chegada = tempo_atual;
	aux.tempo_usado = 0;
	aux.tempo_restante = p->tempo_restante;
	aux.tamanho_na_memoria[0] = p->tamanho_na_memoria[0] + 1;
	aux.tamanho_na_memoria[1] = p->tamanho_na_memoria[1];
	aux.tamanho_no_heap = p->tamanho_no_heap;

	return aux;
}

/**
 * @brief Le em memoria o novo processo, e substitui as instruções do processo que o originou -> exec linux

 * 
 * @param p Processo a substituir
 * @param nome_ficheiro Nome do ficheiro que vai substituir o velho
 * @return Processo Retornamos o processo com as instruções substituidas
 */
Processo execProcesso(Processo *p, char nome_ficheiro[50])
{
	int tempo_heap;
	Processo aux;
	aux.chegada = p->chegada;
	aux.tamanho_na_memoria[0] = contador_memoria - 1;

	char *nome_fich = concatenar_duas_strings(nome_ficheiro, ".prg");

	//Lemos o ficheiro
	FILE *progFile = fopen(nome_fich, "rb");

	// Verificação da abertura do ficheiro
	if (progFile == NULL)
	{
		printf(" Erro na abertura do ficheiro... \n");
		exit(1);
	}

	//Lemos todas as linhas do ficheiro

	char old[50];

	//Ler instruções do ficheiro do processo

	while (fscanf(progFile, "%d", &tempo_heap) == 1)
		{
			int res = dealocar_heap(p->pid);
			if(res == -1){
				printf("\nErro a dealocar a mem�ria\n");
				aux.pid = -2;
				return aux;
			}
			
			int resultado_memoria = GestorMemoria(tempo_heap, p->pid);
			
			if(resultado_memoria == -1){
				printf("\nErro a alocar a mem�ria\n");
				aux.pid = -2;
				return aux;
			}
				
			aux.tamanho_no_heap = tempo_heap;

	while (fscanf(progFile, "%s", old) == 1)
	{

		//Leitura da instrução L
		if (strcmp("L", old) == 0)
		{

			strcpy(&memory[contador_memoria].ins, old);

			char filename_aux[50];
			if (fscanf(progFile, "%s", filename_aux) == 1)
			{

				strcpy(memory[contador_memoria].nome, filename_aux);
			}
		}
		// Leitura da intrução T
		else if (strcmp("T", old) == 0)
		{

			strcpy(&memory[contador_memoria].ins, old);
		}

		else
		{

			strcpy(&memory[contador_memoria].ins, old);

			int numero_aux;

			if (fscanf(progFile, "%d", &numero_aux) == 1)
			{
				memory[contador_memoria].n = numero_aux;
			}
		}

		contador_memoria++;
		memory = (Instrucao *)realloc(memory, (contador_memoria + 1) * sizeof(Instrucao));
	}
	}
	// Fecho do Ficheiro
	fclose(progFile);

	aux.tamanho_na_memoria[1] = contador_memoria;

	//nome do processo
	strcpy(aux.fname, nome_ficheiro);

	//pid
	aux.pid = p->pid;

	//ppid
	aux.ppid = p->ppid;


	//valor
	aux.valor = p->valor;

	//mudar
	aux.priority = p->priority;


	aux.tempo_restante = aux.tamanho_na_memoria[1] - aux.tamanho_na_memoria[0];

	//Tempo usado
	aux.tempo_usado = p->tempo_usado;

	return aux;
}

/**
 * @brief Le um novo processo em memoria
 * 
 * @param nome_ficheiro Nome do processo a ler
 * @param parent Nome do pai do processo
 * @param priority Prioridade do processo
 * @param pc Program counter
 * @param valor Valor 
 * @param tempo_atual Tempo atual
 * @return Processo Processo em memória
 */
Processo criarProcesso(char nome_ficheiro[50], int parent, int priority,  int valor, int tempo_atual)
{
	int tempo_heap;
	Processo aux;
	aux.chegada = tempo_atual;
	aux.tamanho_na_memoria[0] = contador_memoria;

	//Lemos o ficheiro
	FILE *progFile = fopen(nome_ficheiro, "rb");

	// Verificação da abertura do ficheiro
	if (progFile == NULL)
	{
		printf(" Erro na abertura do ficheiro... \n");
		exit(1);
	}

	//Lemos todas as linhas do ficheiro

	char old[50];

	//Ler instruções do ficheiro do processo

	
	while (fscanf(progFile, "%d", &tempo_heap) == 1)
		{
			
			int resultado_memoria = GestorMemoria(tempo_heap, current_pid);
			
			if(resultado_memoria == -1){
				aux.pid = -2;
				return aux;
			}
			
			aux.tamanho_no_heap = tempo_heap;
		
	while (fscanf(progFile, "%s", old) == 1)
	{

		//Leitura da instrução L
		if (strcmp("L", old) == 0)
		{

			strcpy(&memory[contador_memoria].ins, old);

			char filename_aux[50];
			if (fscanf(progFile, "%s", filename_aux) == 1)
			{

				strcpy(memory[contador_memoria].nome, filename_aux);
			}
		}
		// Leitura da intrução T
		else if (strcmp("T", old) == 0)
		{

			strcpy(&memory[contador_memoria].ins, old);
		}

		else
		{

			strcpy(&memory[contador_memoria].ins, old);

			int numero_aux;

			if (fscanf(progFile, "%d", &numero_aux) == 1)
			{
				memory[contador_memoria].n = numero_aux;
			}
		}

		contador_memoria++;
		memory = (Instrucao *)realloc(memory, (contador_memoria + 1) * sizeof(Instrucao));
	}
	}
	
	// Fecho do Ficheiro
	fclose(progFile);

	//Split string by "."
	char nome_splited[50] = "";

	for (int i = 0; i < 50; ++i)
	{

		if ('.' == nome_ficheiro[i])
		{
			break;
		}

		else
		{
			nome_splited[i] = nome_ficheiro[i];
		}
	}

	aux.tamanho_na_memoria[1] = contador_memoria;

	//nome do processo
	strcpy(aux.fname, nome_splited);

	//pid
	aux.pid = current_pid;
	current_pid++;

	//ppid
	aux.ppid = parent;

	
	//valor
	aux.valor = 0;

	//mudar
	aux.priority = priority;


	aux.tempo_restante = aux.tamanho_na_memoria[1] - aux.tamanho_na_memoria[0];

	//Tempo usado
	aux.tempo_usado = 0;

	contador_processos++;
	return aux;
}

/**
 * @brief Leitura do Ficheiro plan.txt
 * 
 * @return int resultado
 */
int lerFicheiroPlan()
{

	int count = 0;

	FILE *progFile = fopen("plan.txt", "rb");

	if (progFile == NULL)
	{
		printf(" Erro na abertura do ficheiro... \n");
		exit(1);
	}

	char old[50];
	int aux;
	int aux2;

	//Ler instruções do ficheiro do processo

	while (fscanf(progFile, "%s", old) == 1)
	{

		strcpy(control_processos[count].name, old);

		if (fscanf(progFile, "%d", &aux) == 1)
		{
			control_processos[count].tempo_chegada = aux;
			//printf("%d\n", control_processos[count].tempo_chegada );
		}

		if (fscanf(progFile, "%d", &aux2) == 1)
		{
			control_processos[count].priority = aux2;
			//printf("%d\n", control_processos[count].tempo_chegada );
		}

		count++;
		control_processos = (Control *)realloc(control_processos, (count + 1) * sizeof(Control));
	}

	// Fecho do ficheiro
	fclose(progFile);

	return count;
}

/**
 * @brief Leitura do ficheiro control.txt
 * 
 * @return int resultado
 */
int lerFicheirosComandos()
{

	FILE *progFile = fopen("control.txt", "rb");

	//verificação na abertura do ficheiro
	if (progFile == NULL)
	{
		printf(" Erro na abertura do ficheiro... \n");
		exit(1);
	}

	char old;

	//Ler os comandos do control.txt
	while (fscanf(progFile, "%c\n", &old) == 1)
	{
		strcpy(&comandos[numero_de_comandos], &old);
		numero_de_comandos++;
		comandos = (char *)realloc(comandos, (numero_de_comandos) * sizeof(char));
	}

	return 1;
}


/**
 * @brief 
 * 
 * @param tempo_atual  
 * @param p Processo em questão
 * @return int 
 */
int turnaround_time(int tempo_atual, Processo *p)
{

	int resultado = tempo_atual - (p->chegada);

	return resultado;
}


/**
 * @brief Quando o processo deixa de ser atual , temos de atualizar as suas variaveis no pcb
 * 
 * @param p_atual Processo atual
 * @param p processo  a atualizar no pcb
 */
void atualizar_processo_no_pcb(Atual *p_atual, Processo *p)
{

	p->pid = p_atual->pid;

	p->valor = p_atual->valor;

	p->tempo_usado = p_atual->tempo_usado;

	p->tempo_restante = p_atual->tempo_restante;
	return;
}

/**
 * @brief Atualização do processo atual

 * 
 * @param p_atual Processo atual
 * @param p Processo para o qual atualizar
 */
void atualizar_processo_atual(Atual *p_atual, Processo *p)
{


	p_atual->pid = p->pid;

	p_atual->ppid = p->ppid;

	p_atual->chegada = p->chegada;

	p_atual->priority = p->priority;

	p_atual->valor = p->valor;

	p_atual->tempo_usado = p->tempo_usado;

	int auxiliar = p->tamanho_na_memoria[1] - p->tamanho_na_memoria[0];

	p_atual->tempo_restante = auxiliar;
}


/**
 * @brief Imprimir as estatisticas globais
 * 
 * @param table PcbTable com todos os processos
 * @param t_around Lista dos turn around times
 */
void print_Global_Stats(Processo *table, Turnarounds t_around)
{

	int tot = 0;
	for (int i = 0; i < t_around.c; i++)
		tot += t_around.array_tempos[i];

	if (t_around.c == 0)
		printf("\n");

	else
		printf("Turnaround time em media: %d \n", (tot / t_around.c));

	printf("--------------------------------------------------\n");

	printf("Nome \t\t|\t Tempo CPU \t|\t PID \t\t|\t Valor \t\t|\t Prioridade \t|\t Tempo restante |\n");
	for (int i = 0; i < current_pid; ++i)
	{

		printf(" %s \t|\t %d \t\t|\t %d \t\t|\t %d \t\t|\t %d \t\t|\t %d \t\t|\t \n", table[i].fname, table[i].tempo_usado, table[i].pid, table[i].valor, table[i].priority, table[i].tempo_restante);
	}
	printf("--------------------------------------------------\n");
	printf("\nEstado da mem�ria:");
	for(int i = 0; i < 5; i++){
		printf("\nBloco nr %d:\n Tamanho Total = %d \n Tamanho Livre = %d \n Tamanho Ocupado = %d \n", i+1, Heap[i].tamanho_total , Heap[i].tamanho_restante, Heap[i].tamanho_ocupado);
		UNIDADE_BLOCO *aux = Heap[i].processo;
		while(aux != NULL){
			if(aux != NULL )
				printf(" pid = %d, tamanho = %d\n", aux->pid, aux->tamanho);
			aux = aux->nseg;
		}
	}
	
}

/**
 * @brief Incializa os blocos de memoria
 * 
 * @param array 
 */
void inicializar_heap(int array[5]){
	for(int i = 0; i < 5; i++){
		Heap[i].tamanho_total = array[i];
		Heap[i].tamanho_ocupado = 0;
		Heap[i].tamanho_restante = array[i];
		Heap[i].processo = NULL;
	}
}




/**
 * @brief função principal do simulador
 * 
 * @param argc Contador de argumentos
 * @param argv argumentos
 * @return int resultado
 */
int main(int argc, char *argv[])
{

	int block_sizes[] = {64, 64, 128, 256, 512};
	inicializar_heap(block_sizes);
	
	
	
	printf("Enter the dynamic memory allocation you want : \n");
	printf("0 - Best Fit - Default \n");
	printf("1 - Worst Fit  \n");
	printf("2 - First Fit  \n");
	printf("3 - Next Fit \n ");
	scanf("%d", &escolha_alocacao);

	printf("Enter the scheduling algorithm you want : \n");
	printf("0 - Round Robin - Default \n");
	printf("1 - FCFS(First Come First Served)  \n");
	printf("2 - Priority  \n");
	printf("3 - SJFS(Shortest Job First Served) \n ");
	scanf("%d", &escolha_escalonamento);



	//Inicializamos o tempo
	int tempo_atual = 0;

	//Variavel que controla a memoria
	int memory_count = 0;

	int control_pid = 0;

	int pid_temporario;

	int random_number;

	Turnarounds t_around;

	t_around.c = 0;
	t_around.array_tempos[0] = 0;

	//Inicializamos a fila da memoria que vai conter as instruções
	memory = (Instrucao *)malloc(sizeof(Instrucao));

	//Incializamos a fila dos processos
	control_processos = (Control *)malloc(sizeof(Control));

	//Inicializamos os comandos
	comandos = (char *)malloc(sizeof(char));

	//Gestor de processos
	Gestor gestor;


	gestor.processo_atual.pid = 0;
	gestor.processo_atual.ppid = -1;
	gestor.processo_atual.priority = 0;
	gestor.processo_atual.chegada = 0;
	gestor.processo_atual.valor = 0;
	gestor.processo_atual.tempo_usado = 0;
	gestor.processo_atual.tempo_restante = 0;

	//Inicalizamos as filas de processos
	gestor.prontos = NULL;
	gestor.processos_a_correr = NULL;
	gestor.processos_bloqueados = NULL;
	gestor.processos_terminados = NULL;
	gestor.processos_concorrentes = NULL;

	//Lemos os processos do fciheir plan.txt
	int numero_de_processos_no_plan = lerFicheiroPlan();

	//Lemos os comandos do control.txt
	lerFicheirosComandos();

	//Controla os processos que ja entraram
	int ja_entraram[100];
	memset(ja_entraram, 0, 100 * sizeof(int));
	
	//Corremos os comandos todos do control.txt
	for (int i = 0; i < numero_de_comandos; i++)
	{
		printf("\n-----------------------------------\n");
		printf("Estou a executar o comando %c \n ", comandos[i]);
		//Verificamos qual comando é
		switch (comandos[i])
		{

		//Corremos o programa para N unidades de tempo
		case 'E':

			memory_count = 0;
			
			//Caso chegue um processo chegue metemo-lo na fila dos gestor.prontos
			for (int j = 0; j < numero_de_processos_no_plan; j++)
			{
				
				if (control_processos[j].tempo_chegada <= tempo_atual)
				{
					if (ja_entraram[j] == 0)
					{
						
						gestor.pcbTable[current_pid] = criarProcesso(control_processos[j].name, -1, control_processos[j].priority,  gestor.processo_atual.valor, control_processos[j].tempo_chegada);
						if(gestor.pcbTable[current_pid].pid == -2){
							continue;
						}
						
						if (escolha_escalonamento == 1)
						{
							/*FCFS
						
							Assim que chega, caso nao esteja nehuum a correr é posto logo a correr.
							Caso haja algum programa a correr , fica nos gestor.prontos e é o próximo a ser executado.

						*/
							//Caso existam prcoesso a correr
							if (gestor.processos_a_correr != NULL)
							{
								Inserir_fila(&gestor.prontos, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								bubbleSortPID(gestor.prontos);
								printf("O processo %s chegou e ficou pronto com o pid %d.\n", control_processos[j].name, gestor.pcbTable[current_pid - 1].pid);
							}
							//Caso nao existam processos a correr
							else
							{
								printf("O processo %s chegou e ficou a correr com o pid %d.\n", control_processos[j].name, gestor.pcbTable[current_pid - 1].pid);
								//Metemos o processo atual na fila dos que estão a correr
								Inserir_fila(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);

								atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[current_pid - 1]);
							}
						}
						/*Priority
						
							Assim que chega, caso nao esteja nehuum a correr é posto logo a correr.
							Caso haja algum programa a correr , então vamos verificar as prioridades de cada processo.

						*/

						else if (escolha_escalonamento == 2)
						{
							//Caso nao existam prcoesso a correr
							if (gestor.processos_a_correr == NULL)
							{
								Inserir_fila(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[current_pid - 1]);
								printf("O processo %s chegou e ficou a correr com o pid %d.\n", control_processos[j].name, gestor.pcbTable[current_pid - 1].pid);
							}
							else
							{
								if (gestor.processo_atual.priority > gestor.pcbTable[current_pid - 1].priority)
								{
									Inserir_fila(&gestor.prontos,gestor.processo_atual.pid, gestor.pcbTable[gestor.processo_atual.pid]);
									Retirar_elemento(&gestor.processos_a_correr);
									atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[current_pid - 1]);
									Inserir_fila(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								}
								else
								{
									Inserir_fila(&gestor.prontos, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								}
								bubbleSortPrio(gestor.prontos);
							}
						}

						/*SJFS
						
							Assim que chega, caso nao esteja nenhum a correr é posto logo a correr.
							Caso haja algum programa a correr , fica nos gestor.prontos e é o próximo a ser executado.

						*/

						else if (escolha_escalonamento == 3)
						{
							if (gestor.processos_a_correr == NULL)
							{
								Inserir_fila(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[current_pid - 1]);
								printf("O processo %s chegou e ficou pronto com o pid %d.\n", control_processos[j].name, gestor.pcbTable[current_pid - 1].pid);
							}
							else
							{
								if (gestor.processo_atual.tempo_restante > gestor.pcbTable[current_pid - 1].tempo_restante)
								{
									Inserir_fila(&gestor.prontos, gestor.processo_atual.pid, gestor.pcbTable[gestor.processo_atual.pid]);
									Retirar_elemento(&gestor.processos_a_correr);
									atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[current_pid - 1]);
									Inserir_fila(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								}
								else
								{
									Inserir_fila(&gestor.prontos, current_pid - 1, gestor.pcbTable[current_pid - 1]);
								}
								bubbleSortSJF(gestor.prontos);
							}
						}
						else
						{

							Inserir_last(&gestor.prontos, current_pid - 1, gestor.pcbTable[current_pid - 1]);
							printf("O processo %s chegou e ficou pronto com o pid %d.\n", control_processos[j].name, gestor.pcbTable[current_pid - 1].pid);
						}
					}
					control_pid++;
					ja_entraram[j] = -1;
				}
			}

			if (gestor.processos_a_correr == NULL && ( (gestor.prontos == NULL && gestor.processos_bloqueados != NULL) ) )
			{
				tempo_atual++;
				printf("So existem processos bloqueados !!!\n");
				continue;
			}

			//Round Robin
			if (gestor.prontos != NULL && (escolha_escalonamento != 1 && escolha_escalonamento != 2 && escolha_escalonamento != 3))
			{
				if (gestor.processos_a_correr != NULL && gestor.processos_a_correr->processo.ppid == -1)
				{
					Inserir_last(&gestor.prontos, gestor.processos_a_correr->pid, gestor.pcbTable[gestor.processos_a_correr->pid]);
					Retirar_elemento(&gestor.processos_a_correr);

					int aux_pid = Retirar_elemento(&gestor.prontos);
					atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[aux_pid]);

					Inserir_fila(&gestor.processos_a_correr, aux_pid, gestor.pcbTable[aux_pid]);
				}
				else
				{
					int aux_pid = Retirar_elemento(&gestor.prontos);
					atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[aux_pid]);

					Inserir_fila(&gestor.processos_a_correr, aux_pid, gestor.pcbTable[aux_pid]);
				}
			}

			//Corremos a memória
			copiarFila(&gestor.processos_a_correr, &gestor.processos_concorrentes);
			
			int aux_tempo = tempo_atual;
			while (gestor.processos_concorrentes != NULL)
			{
				printf("-----------------------------------\n");
				if(gestor.processos_concorrentes->pid != -1)
					printf("\nEsta a correr o processo com pid = %d\n", gestor.processos_concorrentes->pid);
				memory_count = 0;
				atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[gestor.processos_concorrentes->pid]);
				while (memory_count < time_quantum)
				{
					if (gestor.processos_a_correr == NULL || gestor.processo_atual.tempo_restante <= 0)
					{
						tempo_atual++;
						printf("\nSai porque nao haviam processos \n");
						break;
					}
					//Decrementamos o tempo que falta no processo atual
					gestor.processo_atual.tempo_restante--;
					printf("\nInstrucao = %c\n", memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].ins);

					gestor.processo_atual.tempo_usado++;
					tempo_atual++;

					//Verificamos qual é a instrução
					switch (memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].ins)
					{

					//Mudamos o valor do processo atual para o valor da instrução
					case 'M':

						gestor.processo_atual.valor = memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].n;

						printf("O valor do processo atual é  %d \n", gestor.processo_atual.valor);

						break;

					//Adicionamos um valor ao valor atual do processo
					case 'A':

						gestor.processo_atual.valor = gestor.processo_atual.valor + memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].n;

						printf("O valor do processo atual é  %d \n", gestor.processo_atual.valor);

						break;

					//Subtraímos um valor ao valor atual do processo
					case 'S':

						gestor.processo_atual.valor = gestor.processo_atual.valor - memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].n;

						printf("O valor do processo atual é  %d ", gestor.processo_atual.valor);

						break;

					//Bloqueamos o processo atual
					case 'B':
						Retirar_elemento(&gestor.processos_a_correr);

						//Metern no pcb
						atualizar_processo_no_pcb(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);
						printf("Processo bloqueado com o respetivo pid :  %d\n", gestor.processo_atual.pid);

						Inserir_fila(&gestor.processos_bloqueados, gestor.processo_atual.pid, gestor.pcbTable[gestor.processo_atual.pid]);

						break;

					//Terminação do Processo
					case 'T':;
						int res = dealocar_heap(gestor.processos_a_correr->pid);
						if(res == -1)
							printf("\nErro a dealocar a mem�ria\n");
						
						Retirar_elemento(&gestor.processos_a_correr);

						//Turn around time
						int turn_result = turnaround_time(tempo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);

						t_around.array_tempos[t_around.c++] = turn_result;

						Inserir_fila(&gestor.processos_terminados, gestor.processo_atual.pid, gestor.pcbTable[gestor.processo_atual.pid]);

						printf("Processo terminado com sucesso \n   Turn around time : %d \n", turn_result);

						break;

					//Criação de um novo processo
					case 'C':

						//Adicionamos uma cópia do processo velho ao pcb table
						atualizar_processo_no_pcb(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);
						gestor.pcbTable[current_pid] = forkProcesso(&gestor.pcbTable[gestor.processo_atual.pid], tempo_atual);
						if(gestor.pcbTable[current_pid].pid != -2){
							gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0] += memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].n;
							atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);
	
							Inserir_last(&gestor.processos_a_correr, current_pid - 1, gestor.pcbTable[current_pid - 1]);
							//Inserir_last(&processos_con, current_pid - 1, gestor.pcbTable[current_pid - 1]);
	
							printf("\nProcesso filho criado com sucesso , com o pid : %d\n", current_pid - 1);
						}
						else{
							printf("N�o h� espa�o para o fork");
						}
						break;

					//Limpa o processo antigo e substitui-o por filename
					case 'L':

						printf("Processo subtituido por %s \n", memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].nome);

						Processo verificacao_tamanho = execProcesso(&gestor.pcbTable[gestor.processo_atual.pid], memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].nome);
						
						if(verificacao_tamanho.pid != -2){
							atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);
							gestor.pcbTable[gestor.processo_atual.pid] = execProcesso(&gestor.pcbTable[gestor.processo_atual.pid], memory[gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0]].nome);
						}
						
						break;
					}

					//Atualizar a gestor.pcbTable
					atualizar_processo_no_pcb(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);

					//Aumentamos a execução inicial do processo para sabermos onde começar na proxima
					gestor.pcbTable[gestor.processo_atual.pid].tamanho_na_memoria[0] += 1;

					memory_count++;
					printf("\nTempo Restante no atual processo: %d\n", gestor.processo_atual.tempo_restante);


					//scheduling
					//Caso não haja processos em espera para executar então
					if (gestor.prontos == NULL && gestor.processo_atual.tempo_restante > 0)
					{

						//Vamos ver se ainda existe processos a correr
						if (gestor.processos_a_correr != NULL)
						{
							printf("processo a correr\n");

							//Então continuamos com o mesmo processo
						}

						//Caso não existam processos a correr, vamos ver os processo bloqueados

						else if (gestor.processos_bloqueados != NULL)
						{

							//Continuamos para a próxima iteração e esperamos que o processo desbloqueie

							printf("processos bloquados\n");
						}
						else
						{
							return 0;
						}

						//Caso não haja processos em nenhuma das filas então
					}
					else if (gestor.processos_a_correr == NULL)
					{

						pid_temporario = Retirar_elemento(&gestor.prontos);

						atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[pid_temporario]);
						Inserir_fila(&gestor.processos_a_correr, pid_temporario, gestor.pcbTable[pid_temporario]);
					}
					else if (gestor.processo_atual.tempo_restante <= 0)
					{
						printf("acabou o tempo do processo\n");
						
						int res = dealocar_heap(gestor.processo_atual.pid);
						if(res == -1)
							printf("\nErro a dealocar a mem�ria\n");
						
						if (gestor.processo_atual.pid == gestor.processos_a_correr->pid)
							Retirar_elemento(&gestor.processos_a_correr);
						else
							Retirar_elemento_especifico(&gestor.processos_a_correr, gestor.processo_atual.pid);

						atualizar_processo_no_pcb(&gestor.processo_atual, &gestor.pcbTable[gestor.processo_atual.pid]);

						Inserir_fila(&gestor.processos_terminados, gestor.processo_atual.pid, gestor.pcbTable[gestor.processo_atual.pid]);

						if (gestor.processos_a_correr == NULL)
						{

							int pid_aux = Retirar_elemento(&gestor.prontos);
							if (pid_aux != -1)
							{
								atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[pid_aux]);
								Inserir_fila(&gestor.processos_a_correr, pid_aux, gestor.pcbTable[pid_aux]);
							}
						}
						else if (gestor.processos_a_correr != NULL)
						{
							atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[gestor.processos_a_correr->pid]);
						}
					}
					else if (gestor.processo_atual.tempo_restante <= 0)
					{
						printf("\nA espera de processos\n");
					}
				}
				gestor.processos_concorrentes = gestor.processos_concorrentes->nseg;
				tempo_atual = aux_tempo;
				tempo_atual += 2;
				printf("-----------------------------------\n");
				if(gestor.processos_concorrentes != NULL){
					atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[gestor.processos_concorrentes->pid]);
				}
			}

			break;

		// Interromper o processo que está em execução e bloquea-lo
		case 'I':;
			int pid_retirado = Retirar_elemento(&gestor.processos_a_correr); 
			if (pid_retirado != -1)
			{
				atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[pid_retirado]);
				atualizar_processo_no_pcb(&gestor.processo_atual, &gestor.pcbTable[pid_retirado]);
				printf("Processo com pid %d foi bloqueado\n", pid_retirado);
				Inserir_fila(&gestor.processos_bloqueados, pid_retirado, gestor.pcbTable[pid_retirado]);
				if(gestor.processos_a_correr == NULL){
					int pid_a = Retirar_elemento(&gestor.prontos);
					if (pid_a != -1)
					{
						atualizar_processo_atual(&gestor.processo_atual, &gestor.pcbTable[pid_a]);
						Inserir_fila(&gestor.processos_a_correr, pid_a, gestor.pcbTable[pid_a]);
					}
				}
			}

			//Metern no pcb

			break;

		// Executar o escalonador ( desbloquear processos )
		case 'D':

			srand(time(&t));

			FILA *aux_bloqueados = NULL;

			copiarFila(&gestor.processos_bloqueados, &aux_bloqueados);

			while (aux_bloqueados != NULL)
			{

				random_number = rand() % 100;
				printf("Saiu o numero %d!\n", random_number);
				if (random_number <= 33)
				{
					Inserir_last(&gestor.prontos, aux_bloqueados->pid, aux_bloqueados->processo);

					Retirar_elemento_especifico(&gestor.processos_bloqueados, aux_bloqueados->pid);
					printf("Processo desbloqueado!");
				}

				aux_bloqueados = aux_bloqueados->nseg;
			}

			break;

		// Imprimir estatísticas
		case 'R':
			printf("\n\nREPORTAGEM:\n");
			printf("TEMPO ATUAL: %d\n", tempo_atual);
			printf("\n-----------------------------\n");
			printf("\nPROCESSO EM EXECUCAO:");
			FILA *aux_print = gestor.processos_a_correr;
			if(aux_print == NULL || gestor.pcbTable[aux_print->pid].tempo_restante <= 0)
				printf("\nNAO HA PROCESSOS EM EXECUCAO\n");
			else{
				printf("\npid | ppid | Priority | Valor | Chegada | Tempo CPU | Tempo restante |\n");
				while (aux_print != NULL && gestor.pcbTable[aux_print->pid].tempo_restante > 0)
				{
					printf(" %d  |  %d  |    %d    |  %d   |    %d   |    %d     |      %d        |\n", gestor.pcbTable[aux_print->pid].pid, gestor.pcbTable[aux_print->pid].ppid, gestor.pcbTable[aux_print->pid].priority, gestor.pcbTable[aux_print->pid].valor, gestor.pcbTable[aux_print->pid].chegada, gestor.pcbTable[aux_print->pid].tempo_usado, gestor.pcbTable[aux_print->pid].tempo_restante);
					aux_print = aux_print->nseg;
				}
			}
			printf("\n-----------------------------\n");
			printf("\nPROCESSOS BLOQUEADOS:");
			aux_print = gestor.processos_bloqueados;
			printf("\npid | ppid | Priority | Valor | Chegada | Tempo CPU | Tempo restante |\n");
			while (aux_print != NULL)
			{
				printf(" %d  |  %d  |    %d    |  %d   |    %d   |    %d     |      %d        |\n", gestor.pcbTable[aux_print->pid].pid, gestor.pcbTable[aux_print->pid].ppid, gestor.pcbTable[aux_print->pid].priority, gestor.pcbTable[aux_print->pid].valor, gestor.pcbTable[aux_print->pid].chegada, gestor.pcbTable[aux_print->pid].tempo_usado, gestor.pcbTable[aux_print->pid].tempo_restante);
				aux_print = aux_print->nseg;
			}
			printf("\n-----------------------------\n");
			printf("\nPROCESSOS prontos A EXECUTAR:");
			aux_print = gestor.prontos;
			printf("\npid | ppid | Priority | Valor | Chegada | Tempo CPU |\n");
			while (aux_print != NULL)
			{
				printf("  %d  |  %d  |    %d    |  %d   |    %d   |    %d     |\n", gestor.pcbTable[aux_print->pid].pid, gestor.pcbTable[aux_print->pid].ppid, gestor.pcbTable[aux_print->pid].priority, gestor.pcbTable[aux_print->pid].valor, gestor.pcbTable[aux_print->pid].chegada, gestor.pcbTable[aux_print->pid].tempo_usado);
				aux_print = aux_print->nseg;
			}
			printf("\n-----------------------------\n");
			printf("\nPROCESSOS TERMINADOS:");
			aux_print = gestor.processos_terminados;
			printf("\npid | ppid | Priority | Valor | Chegada | Tempo CPU\n");
			while (aux_print != NULL)
			{
				printf("  %d  |  %d  |    %d    |  %d   |    %d   |    %d     |\n", gestor.pcbTable[aux_print->pid].pid, gestor.pcbTable[aux_print->pid].ppid, gestor.pcbTable[aux_print->pid].priority, gestor.pcbTable[aux_print->pid].valor, gestor.pcbTable[aux_print->pid].chegada, gestor.pcbTable[aux_print->pid].tempo_usado);
				aux_print = aux_print->nseg;
			}
			printf("\n-----------------------------\n");
			printf("\nFIM REPORTAGEM\n\n");
			break;

		//Terminar o simulador e imprimir estatísticas globais
		case 'T':

			print_Global_Stats(gestor.pcbTable, t_around);

			return 0;
			break;
		}
		printf("\n-----------------------------------\n\n");
	}
}
