# Simulador de Gestão de Processos Escalonamento e Gestão de Memoria


> Para este trabalho foi nos proposto desenvolver uma aplicação que simule o
> funcionamento das operações de gestão e execução de processos por um sistema operativo
> incluindo as operações de escalonamento do CPU, criação e terminação de processos e a
> comutação de contexto.

---

## 1. Content table

- [2. Instalação](#Instalação) 
  - [2.1 Clone](#clone)
  - [2.2 Compilar](#Compile)
  - [2.3 Exectuar](#Exectuar)
- [4. Documentação](#Documentação) 
- [4. Team](#Team) 





## 2. Instalação

### 2.1 Clone

```shell
$ git clone https://gitlab.com/P1C4/projetoso.git
```

### 2.2 Compilar

```shell
make
```

### 2.3 Exectuar

```shell
./simulador
```



### 3.Documentação

Para aceder a documentação deste projeto basta entrar na diretoria html e abrir a página, docs.html, num browser á sua escolha e terá acesso á documentação do projeto.



## 4. Team

- António Sardinha
  [![https://gitlab.com/ToniSardinha64](https://i.imgur.com/hLDRZ39.png)](https://gitlab.com/ToniSardinha64) 

- Mauro Gaudêncio
  [![https://gitlab.com/Mielikki26](https://i.imgur.com/hLDRZ39.png)](https://gitlab.com/Mielikki26) 

- Pedro Brito
  [![https://gitlab.com/P1C4](https://i.imgur.com/hLDRZ39.png)](https://gitlab.com/P1C4) 

---